#coding: UTF-8
import common
import csv

# 日本語記事を網羅できるであろう大元のurl。日本語記事を取得するために、これが全てとは限らない。
# 大元のurlたちをデータ化できる場合、データから引っ張ってくる形式にしたい
country = "ja"
xml_url = "http://tripadvisor-sitemaps.s3-website-us-east-1.amazonaws.com/2/ja/sitemap_ja_index.xml"
path = "data/"
# xml.gzのurlのみを切り出し
soup_list = common.get_souplist(xml_url,xml_file=None)

# xml.gzのタグを取得
country_flg = "-" + country +"-"
for soup_ele in soup_list:
    index = soup_ele.find(country_flg)
    if index != 1:
        index_start = index + 4
        index_end = soup_ele[index_start:].find("-") + index_start
        tag = soup_ele[index_start:index_end]
    else:
        country_flg = "not found"
        tag = "not found"
        print(country_flg + " is not found")

    file_name = path + country + "_xmlfile_url.csv"
    with open(file_name,'a') as f:
        writer = csv.writer(f)
        writer.writerow([soup_ele,country,tag])
