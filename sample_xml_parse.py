#coding: UTF-8
import os
import csv
import common
import xml.etree.ElementTree as ET

def split_and_write_filename(domain,soup_list):
    for soup_ele in soup_list:
        index_start = len(domain)
        index_end = len(soup_ele)-5
        filename = soup_ele[index_start:index_end]
        filename_structure = [soup_ele] + filename.split("-")
        writer.writerow(filename_structure)

def write_csv(soup_list,xmlfile,csv_path,domain):
    if soup_list is not None:
        index_end = xmlfile.find(".xml")
        outputfile_name = csv_path + xmlfile[:index_end] + '.csv'
        with open(outputfile_name,'a') as f:
            writer = csv.writer(f)
            split_and_write_filename(domain,soup_list)

# url情報を取り出す対象となるxmlファイルを取得
path = "/Users/HalloHawk/travellers_coin/data/url_data/"
country = "ja"
tag = "hotel_review"
domain = 'https://www.tripadvisor.jp/'

xml_path = path + "xmlfile/" + country + "/" + tag + "/"
csv_path = path + "csv/" + country + "/" + tag + "/"

xmlfile_list  = os.listdir(xml_path)
for xmlfile in xmlfile_list:
    soup_list = common.get_souplist(None, xml_path + xmlfile)
    write_csv(soup_list,xmlfile,csv_path,domain)
