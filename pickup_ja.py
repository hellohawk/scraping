# coding:UTF-8
import os
import csv
import common

def check_flg_write_csv(outputfile_name,reader,check_words,country):
    with open(outputfile_name,'a') as g:
        writer = csv.writer(g)
        for row in reader:
            flg = ["-"]
            if len(row) == 7:
                for check_word in check_words:
                    if row[6].find(check_word) != -1:
                        if check_word == 'Hokkaido' or check_word == 'Osaka':
                            flg = [country]
                        else:
                            if row[6].find("refecture") != -1:
                                flg = [country]
            add_row = [row[0]] + flg
            writer.writerow(add_row)

path = "/Users/HalloHawk/travellers_coin/data/"
data = "ja_prefecture.csv"
use_data = path + data

country = "ja"
tag = "hotel_review"
csv_path = path + "url_data/csv/" + country + "/" + tag + "/"

check_words =  common.check_words_from_data(use_data,0)
csvfile_list = common.file_list_from_data(csv_path,"csv")

for csvfile in csvfile_list:
    file_path = csv_path + csvfile
    index_end = csvfile.find(".csv")
    outputfile_name = path + "url_data/csv_country_flg/" + country + "/" + tag + "/" + csvfile[:index_end] + 'country_flg.csv'
    with open(file_path, 'r') as f:
        reader = csv.reader(f)
        check_flg_write_csv(outputfile_name,reader,check_words,country)
