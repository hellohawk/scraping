#coding: UTF-8
import os
import csv
import sys
import urllib.request
from bs4 import BeautifulSoup

def get_souplist(xml_url=None,xml_file=None):
    ## inputチェック
    # xml_fileを読み込み、loc要素のurlを取得
    if xml_file is not None:
        xml_file_markup = open(xml_file,'r')
        print("xml_file is not None")
    # xml_urlから読み込み、xml_fileを取得
    elif xml_url is not None:
        xml_file_markup = urllib.request.urlopen(xml_url)
        print("xml_url is not None")
    else:
        print("get_souplist input error: there is not xml_url or xml_file")
        return None

    # beautifulsoupを使ってurlを取得
    soup = BeautifulSoup(xml_file_markup, "xml")
    soup_list_with_tag = soup.find_all("loc")
    # fileの中身が正しいかのチェック
    check_len = len(soup_list_with_tag) > 0
    check_text = soup.find("loc").text is not None

    if check_len & check_text == True:
        soup_list = []
        for soup_ele in soup_list_with_tag:
            soup_list += [soup_ele.text]

        return soup_list
    else:
        print("get_souplist file error: this file is not right")
        return None

def check_words_from_data(use_data,row_num):
    with open(use_data, 'r') as f:
        reader = csv.reader(f)
        check_words = []
        for row in reader:
            check_words += [row[row_num]]

        return check_words

def file_list_from_data(file_path,extension):
    file_list_with_noise  = os.listdir(file_path)
    file_list = []
    for file in file_list_with_noise:
        if file.find("." + extension) != -1:
            file_list += [file]

    return file_list

def make_directory(dir_path):
    try:
        os.makedirs(dir_path, exist_ok=True)
    except FileExistsError as e:
        print(e.strerror)  # エラーメッセージ ('Cannot create a file when that file already exists')
        print(e.errno)     # エラー番号 (17)
        print(e.filename)  # 作成できなかったディレクトリ名 ('foo')
        sys.exit(1)
