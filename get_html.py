# coding:UTF-8
import os
import csv
import time
import common
import urllib.request
from bs4 import BeautifulSoup
from selenium import webdriver

def save_html(html_path,domain,html,i):
    common.make_directory(html_path)
    index_start = len(domain)
    index_end = url.find(".html")
    output_path = html_path + "/" + url[index_start:index_end] + "_" + str(i+1) + ".html"
    with open(output_path, mode = 'w', encoding = 'utf-8') as fw:
        fw.write(html.prettify())

def check_repeat_num(browser,url):
    # reviews_countは先にとれる
    reviews_count_text = browser.find_elements_by_xpath('//span[@class="reviews_header_count block_title"]')
    reviews_count_text = reviews_count_text[0].text
    index_end = len(reviews_count_text) - 1
    reviews_count = int(reviews_count_text[1:index_end])
    if reviews_count > 0:
        if reviews_count % 5 > 0:
            repeat_num = int(reviews_count/5) + 1
        else:
            repeat_num = int(reviews_count/5)
    else:
        repeat_num = 0
    return repeat_num

def btn_click(btn):
    for i in range(3):
        try:
            btn.click()
        except Exception as e:
            print("error and retry")
            time.sleep(2) ## 待つ
        else:
            break
    else:
        pass

def open_html(browser,url,repeat_num):
    if repeat_num > 0: ## コメントがある場合はデータを入手
        for i in range(0,repeat_num):
            print("there is some comments")
            time.sleep(3) ## 待つ
            page_source = browser.page_source
            if page_source.find(tsuzuki_btn) != -1:
                btn = browser.find_element_by_css_selector('.taLnk.ulBlueLinks')
                btn_click(btn)
            time.sleep(5) ## 待つ
            page_source = browser.page_source
            html = BeautifulSoup(page_source, 'html.parser')
            save_html(html_path,domain,html,i)
            if page_source.find(next_btn) != -1:
                btn = browser.find_element_by_css_selector('.nav.next.taLnk')
                btn_click(btn)
            else:
                break

path = "data/url_data/"
row_num = 1
country = "ja"
tag = "hotel_review"
csv_path = path + "csv_country_flg/" + country + "/" + tag + "/"
domain = 'https://www.tripadvisor.jp/'

DRIVER_PATH = os.path.join(os.path.abspath(os.path.dirname('travellers_coin')), 'chromedriver')
tsuzuki_btn = '<span class="taLnk ulBlueLinks"'
last_btn = '<span class="nav next disabled">'
next_btn = "nav next taLnk"

use_urls = []
csvfile_list = common.file_list_from_data(csv_path,"csv")
for csvfile in csvfile_list:
    use_data = csv_path + csvfile
    index_end = csvfile.find(".csv")-11
    html_path = path + "htmls/" + country + "/" + tag + "/" + csvfile[:index_end]
    #pickup_url_from_data(use_data,row_num,country,html_path,domain)

    browser = webdriver.Chrome(DRIVER_PATH)
    with open(use_data, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            if row[row_num] == country:
                url = row[0]
                browser.get(url) # URLへアクセス
                for j in range(3):
                    try:
                        repeat_num = check_repeat_num(browser,url)
                    except:
                        print("error and retry")
                        browser.get(url)
                    else:
                        break
                else:
                    pass
                open_html(browser,url,repeat_num)
