#coding:utf-8
import os
import csv
import common
from bs4 import BeautifulSoup

def check_japan(htmlfile,check_words):
    check_japan_result = False
    for check_word in check_words:
        if htmlfile[0].find(check_word) != -1:
            if check_word == 'Hokkaido' or check_word == 'Osaka':
                check_japan_result = True
            else:
                if htmlfile[0].find("refecture") != -1:
                    check_japan_result = True

    return check_japan_result

def make_htmlfolder_list_from_csv(csvfile_path):
    csvfile_list = common.file_list_from_data(csvfile_path,"csv")
    htmlfolder_list_from_csv = []
    for csvfile in csvfile_list:
        index_end = csvfile.find(".csv")
        htmlfolder_list_from_csv += [csvfile[:index_end]]

    return htmlfolder_list_from_csv

def make_htmlfolder_list_is_existed(htmlfolder_path,csvfile_path):
    htmlfolder_list_from_data = os.listdir(htmlfolder_path)
    htmlfolder_list_from_csv = make_htmlfolder_list_from_csv(csvfile_path)
    htmlfolder_list = []
    for htmlfolder_from_csv in htmlfolder_list_from_csv:
        for htmlfolder_from_data in htmlfolder_list_from_data:
            if htmlfolder_from_data == htmlfolder_from_csv:
                htmlfolder_list += [htmlfolder_from_csv]
                break

    return htmlfolder_list

def htmlfile_list_from_htmlfolder_list(htmlfolder_path,htmlfolder_list):
    htmlfiles = []
    if len(htmlfolder_list) > 2:
         for htmlfolder in htmlfolder_list:
             htmlfile_path = htmlfolder_path + htmlfolder
             htmlfile_list = common.file_list_from_data(htmlfile_path,"html")
             times = len(htmlfile_list)
             for time in range(0,times):
                 htmlfiles += htmlfile_path + "/" + htmlfile_list[time]
    elif len(htmlfolder_list) == 1:
        htmlfile_path = htmlfolder_path + htmlfolder_list[0]
        htmlfile_list = common.file_list_from_data(htmlfile_path,"html")
        times = len(htmlfile_list)
        for time in range(0,times):
            htmlfiles += htmlfile_path + "/" + str(htmlfile_list[time])
    else:
        htmlfiles = None
        print("input error : htmlpath is not righr or there is not htmlfile")

    return htmlfiles

def check_reviews_count_is_existed(soup):
    check_reviews_is_existed = soup.find(class_="reviews_header_count block_title") is not None

    return check_reviews_is_existed

def check_reviews_count(soup):
    reviews_count_text = soup.find(class_="reviews_header_count block_title").text
    index_start = reviews_count_text.find("(") + 1
    index_end = reviews_count_text.find(")")
    reviews_count = int(reviews_count_text[index_start:index_end])

    return reviews_count

def check_tsuzuki_btn_is_pushed(soup):
    # 続きボタン押したかのチェック
    tsuzuki_checks = soup.find_all(class_='taLnk ulBlueLinks')
    push_tsuzuki = []
    for tsuzuki_check in tsuzuki_checks:
        push_tsuzuki += [int(tsuzuki_check.text.replace(' ', '').replace('\n','')  == '一部のみ表示する')]

    return push_tsuzuki

def check_all_tsuzuki_btn_is_pushed(push_tsuzuki):
    nomiss_tsuzuki_btn = True
    for push_flg in push_tsuzuki:
        if push_flg == 0:
            nomiss_tsuzuki_btn = False

    return nomiss_tsuzuki_btn

def check_comments(soup):
    comments_checks = soup.find_all("p",class_="partial_entry")
    use_comments = []
    for comments_check in comments_checks:
        use_comments += [comments_check.text.replace(' ', '').replace('\n','')]

    return use_comments

def check_thanks(soup):
    thanks_checks = soup.find_all('span',class_='numHelp emphasizeWithColor')
    use_thanks = []
    for thanks_check in thanks_checks:
        use_thank = thanks_check.text.replace(' ', '').replace('\n','')
        if use_thank == '':
            use_thank = 0
        use_thanks += [int(use_thank)]

    return use_thanks

def check_data_is_right(use_comments,use_thanks,check_num):
    check_1 = len(use_comments) == check_num
    check_2 = len(use_thanks) == check_num
    check_result = check_1 & check_2

    return check_result

def parser(html_path):
    html_markup = open(html_path,'r')
    soup = BeautifulSoup(html_markup, "lxml")

    # カウント数
    check_reviews_is_existed = check_reviews_count_is_existed(soup)
    reviews_count = check_reviews_count(soup)
    push_tsuzuki = check_tsuzuki_btn_is_pushed(soup)
    nomiss_tsuzuki_btn = check_all_tsuzuki_btn_is_pushed(push_tsuzuki)
    use_comments = check_comments(soup)
    use_thanks = check_thanks(soup)

    return  [check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,push_tsuzuki,use_comments,use_thanks]

def check_data_from_parser(check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,use_comments,use_thanks):
    check_num = 0
    if check_reviews_is_existed & nomiss_tsuzuki_btn :
        if reviews_count > 0 & len(use_comments):
            check_num = len(use_comments)
            check_result = check_data_is_right(use_comments,use_thanks,check_num)
            if check_result:
                check_data_result = True
            else:
                check_data_result = False
        else:
            check_data_result = False
    else:
        check_data_result = False

    return [check_num,check_data_result]

def pickup_data(use_comments,use_thanks):
    data = []
    for i in range(0,check_num):
        data += [[use_comments[i],use_thanks[i]]]
    return data

def write_csv(path,outputname_base,date,datas,error_datas):
    filepath_datas = path + "output_data/" + "datas/"
    filepath_error_datas = path + "output_data/" + "error_datas/"
    common.make_directory(filepath_datas)
    common.make_directory(filepath_error_datas)
    filename_datas = filepath_datas + outputname_base + "datas" + "_" + date + ".csv"
    filename_error_datas = filepath_error_datas + outputname_base + "error_datas" +"_" + date + ".csv"

    with open(filename_datas,'a',encoding="utf-8") as f:
        csvWriter_datas = csv.writer(f)
        last_datas = len(datas)
        if last_datas == 1:
            csvWriter_error_datas.writerow([datas])
        elif last_datas > 1:
            for last_data in range(0,last_datas):
                csvWriter_datas.writerow(datas[last_data])
        else:
            print("not data")

    with open(filename_error_datas,'a',encoding="utf-8") as g:
        csvWriter_error_datas = csv.writer(g)
        last_error_datas = len(error_datas)
        if last_error_datas == 1:
            csvWriter_error_datas.writerow([error_datas])
        elif last_error_datas > 1:
            for last_error_data in range(0,last_error_datas):
                csvWriter_error_datas.writerow([error_datas[last_error_data]])
        else:
            print("not errordata")

# config 1
path = "/Users/HalloHawk/travellers_coin/data/"
country = "ja"
tag = "hotel_review"
csvfile_path = path + "url_data/csv/" + country + "/" + tag + "/"
htmlfolder_path = path + "url_data/htmls/" + country + "/" + tag + "/"
# config 2
#tsuzuki_btn_tag =
tsuzuki_btn_class = 'taLnk ulBlueLinks'
last_btn_tag = 'span'
last_btn_class = 'nav next disabled"'
#next_btn_tag =
next_btn_class = 'nav next arrowNav taLnk'
# config 3
outputname_base = 'get_sentense_'
date = "20170920"
# config 4
check_words =  common.check_words_from_data("/Users/HalloHawk/travellers_coin/data/ja_prefecture.csv",0)


##
htmlfolder_list = make_htmlfolder_list_is_existed(htmlfolder_path,csvfile_path)
htmlfiles = htmlfile_list_from_htmlfolder_list(htmlfolder_path,htmlfolder_list)

datas = []
error_datas = []
for htmlfile in htmlfiles:
    check_japan_result = check_japan(htmlfile,check_words)
    if check_japan_result:
        html_path = htmlfolder_path + htmlfile[0]
        check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,push_tsuzuki,use_comments,use_thanks = parser(html_path)
        check_num,check_data_result = check_data_from_parser(check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,use_comments,use_thanks)
        if check_data_result:
            data = pickup_data(use_comments,use_thanks)
            datas += data
        else:
            error_datas += [str(htmlfile)]
    else:
        error_datas += [str(htmlfile)]
write_csv(path,outputname_base,date,datas,error_datas)


# htmlfile = "Hotel_Review-g298116-d1087898-Reviews-Hoshi-Komatsu_Ishikawa_Prefecture_Chubu_1.html"
# html_path = "/Users/HalloHawk/travellers_coin/data/url_data/htmls/ja/hotel_review/sitemap-412425-ja-hotel_review-1499526683/" + htmlfile
# check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,push_tsuzuki,use_comments,use_thanks = parser(html_path)
# print("check_reviews_is_existed")
# print(check_reviews_is_existed)
# print("nomiss_tsuzuki_btn")
# print(nomiss_tsuzuki_btn)
# print("reviews_count")
# print(reviews_count)
# print("push_tsuzuki")
# print(push_tsuzuki)
# print("use_comments")
# print(use_comments)
# print("use_thanks")
# print(use_thanks)
# check_num,check_data_result = check_data_from_parser(check_reviews_is_existed,nomiss_tsuzuki_btn,reviews_count,use_comments,use_thanks)
# print("check_num")
# print(check_num)
# print("check_data_result")
# print(check_data_result)
# datas = []
# error_datas = []
# if check_data_result:
#     datas = pickup_data(use_comments,use_thanks)
# else:
#     error_datas = [htmlfile]
# print("datas")
# print(datas)
# print("error_datas")
# print(error_datas)
# write_csv(path,outputname_base,date,datas,err